Name:          libisofs
Version:       1.5.6
Release:       1
Summary:       Library for Creating ISO-9660 Filesystems
License:       GPLv2+ and LGPLv2+
URL:           http://libburnia-project.org/
Source0:       http://files.libburnia-project.org/releases/libisofs-%{version}.tar.gz
Source1:       http://files.libburnia-project.org/releases/libisofs-%{version}.tar.gz.sig
Patch0001:     libisofs-0.6.16-multilib.patch
BuildRequires: libacl-devel zlib-devel gcc

%description
Libisofs is a library to create an ISO-9660 filesystem and supports
extensions like RockRidge or Joliet. It is also a full featured
ISO-9660 editor, allowing you to modify an ISO image or multisession
disc, including file addition or removal, change of file names and
attributes etc.
Libisofs supports the extension AAIP which allows to store ACLs and xattr in
ISO-9660 filesystems and zisofs compression which is transparently uncompressed
by some Linux kernels. It is possible to have data file content compressed to
gzip format or to have it filtered by an external process.

%package devel
Summary:       Development files for libisofs
Requires:      libisofs = %{version}-%{release} pkgconfig

%description   devel
The package provides libraries and header files for developing
applications which use libisofs.

%package       help
Summary:       Documentation files for libisofs
BuildArch:     noarch
BuildRequires: doxygen graphviz
Provides:      libisofs-doc = %{version}-%{release}
Obsoletes:     libisofs-doc < %{version}-%{release}

%description   help
This package contains the API documentation for developing applications that use libisofs.

%prep
%autosetup -n libisofs-%{version} -p1

%build
%configure --disable-static
%make_build
doxygen doc/doxygen.conf

%install
%make_install

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%doc AUTHORS COPYRIGHT README COPYING
%{_libdir}/libisofs*.so.*
%exclude %{_libdir}/libisofs.la
%exclude %{_defaultdocdir}

%files devel
%{_includedir}/libisofs/
%{_libdir}/pkgconfig/libisofs*.pc
%{_libdir}/libisofs.so

%files help
%doc doc/html/

%changelog
* Wed Aug 02 2023 wangce <wangce@uniontech.com> - 1.5.6-1
- Upgrade to version 1.5.6

* Thu Jun 16 2022 SimpleUpdate Robot <tc@openeuler.org> - 1.5.4-1
- Upgrade to version 1.5.4

* Tue Jun 08 2021 wulei <wulei80@huawei.com> - 1.4.8-6
- fixes failed: error: no acceptable C compiler found in $PATH

* Sat Dec 21 2019 Tianfei <tianfei16@huawei.com> - 1.4.8-5
- Package init
